package com.mycompany.validarclave;

import org.junit.Test;
import static org.junit.Assert.*;

public class PasswordTest {
    
    @Test(expected=IllegalArgumentException.class)
    public void validarPassword_PasswordVacio_IllegalArgumentException() {
        Password.validarPassword("");
    }
    
    @Test(expected=NullPointerException.class)
    public void validarPassword_PasswordNull_NullPointerException() {
        Password.validarPassword(null);
    }
    
    @Test
    public void validarPassword_CantidadDeCaracteresMenorQueLoPermitido_false() {
        String password = "holaaa1";
        boolean resultadoEsperado = false;
        boolean resultadoObtenido = Password.validarPassword(password);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void validarPassword_CantidadDeCaracteresMayorQueLoPermitido_false() {
        String password = "hola1aaa22222kkkkkkk8";
        boolean resultadoEsperado = false;
        boolean resultadoObtenido = Password.validarPassword(password);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void validarPassword_CantidadDeCaracteresIgualAlMinimoPermitidoConCaracterNumerico_true() {
        String password = "ddddddd1";
        boolean resultadoEsperado = true;
        boolean resultadoObtenido = Password.validarPassword(password);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void validarPassword_CantidadDeCaracteresIgualAlMaximoPermitidoConCaracterNumerico_true() {
        String password = "qqqqqqq2qqqqqqqqqqqq";
        boolean resultadoEsperado = true;
        boolean resultadoObtenido = Password.validarPassword(password);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void validarPassword_SinCaracterNumerico_false() {
        String password = "dddddddwr";
        boolean resultadoEsperado = false;
        boolean resultadoObtenido = Password.validarPassword(password);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void validarPassword_CantidadDeCaracteresDentroDeLoPermitidoConCaracterNumerico_true() {
        String password = "jdke56kdl0";
        boolean resultadoEsperado = true;
        boolean resultadoObtenido = Password.validarPassword(password);
        assertEquals(resultadoEsperado, resultadoObtenido);

    }
}
