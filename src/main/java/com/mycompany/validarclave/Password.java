
package com.mycompany.validarclave;

public class Password {

    public static boolean validarPassword(String password) {
        if(password.length()==0) {
            throw new IllegalArgumentException("Password Vacío");
        }
        if (password.length() >= 8 && password.length() <= 20) {

            for (int i = 0; i < password.length(); i++) {

                if (Character.isDigit(password.charAt(i))) {
                    return true;
                }
            }            
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.print(Password.validarPassword(null));
    }
}
